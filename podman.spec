%global with_check 0

# https://bugzilla.redhat.com/show_bug.cgi?id=1904567
%global _lto_cflags %%{nil}

%global _find_debuginfo_dwz_opts %{nil}
%global _dwz_low_mem_die_limit 0

%if 0%{?rhel} > 7 && ! 0%{?fedora}
%define gobuild(o:) \
go build -buildmode pie -compiler gc -tags="rpm_crashtraceback libtrust_openssl ${BUILDTAGS:-}" -ldflags "${LDFLAGS:-} -linkmode=external -compressdwarf=false -B 0x$(head -c20 /dev/urandom|od -An -tx1|tr -d ' \\n') -extldflags '%__global_ldflags'" -a -v %{?**};
%else
%if ! 0%{?gobuild:1}
%define gobuild(o:) GO111MODULE=off go build -buildmode pie -compiler gc -tags="rpm_crashtraceback ${BUILDTAGS:-}" -ldflags "${LDFLAGS:-} -linkmode=external -B 0x$(head -c20 /dev/urandom|od -An -tx1|tr -d ' \\n') -extldflags '-Wl,-z,relro -Wl,-z,now -specs=/usr/lib/rpm/redhat/redhat-hardened-ld '" -a -v %{?**};
%endif
%endif

%global import_path github.com/containers/podman
%global branch master
%global commit0 e2f51eeb0693eda026fa509a9decfbdd7e0b74a8
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global cataver 0.1.5
%global dnsnamever 1.1.1

Name: podman
Version: 3.3.0
Release: 0.6%{?dist}
Summary: Manage Pods, Containers and Container Images
License: ASL 2.0 and GPLv3+
URL: https://%{name}.io/
%if 0%{?branch:1}
Source0: https://%{import_path}/tarball/%{commit0}/%{branch}-%{shortcommit0}.tar.gz
%else
Source0: https://%{import_path}/archive/%{commit0}/%{name}-%{version}-%{shortcommit0}.tar.gz
%endif
Source1: https://github.com/openSUSE/catatonit/archive/v%{cataver}.tar.gz
Source2: https://github.com/containers/dnsname/archive/v%{dnsnamever}.tar.gz
# https://fedoraproject.org/wiki/PackagingDrafts/Go#Go_Language_Architectures
ExclusiveArch: %{go_arches}
Provides: %{name}-manpages = %{version}-%{release}
Obsoletes: %{name}-manpages < %{version}-%{release}
BuildRequires: golang >= 1.12.12-4
BuildRequires: glib2-devel
BuildRequires: glibc-devel
BuildRequires: glibc-static
BuildRequires: git-core
BuildRequires: go-md2man
BuildRequires: gpgme-devel
BuildRequires: libassuan-devel
BuildRequires: libgpg-error-devel
BuildRequires: libseccomp-devel
BuildRequires: libselinux-devel
BuildRequires: ostree-devel
BuildRequires: pkgconfig
BuildRequires: make
BuildRequires: systemd
BuildRequires: systemd-devel
# for catatonit
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: file
BuildRequires: gcc
BuildRequires: libtool
Requires: containers-common >= 0.1.29-3
Requires: containernetworking-plugins >= 0.9.1-1
Requires: iptables
Requires: nftables
Obsoletes: oci-systemd-hook < 1
Requires: libseccomp >= 2.4.1
Requires: conmon >= 2.0.25
Requires: (container-selinux if selinux-policy)
Requires: slirp4netns >= 0.4.0-1
Requires: runc >= 1.0.0-57
Requires: fuse-overlayfs
Requires: %{name}-catatonit >= %{version}-%{release}
Requires: oci-runtime

%description
%{name} (Pod Manager) is a fully featured container engine that is a simple
daemonless tool.  %{name} provides a Docker-CLI comparable command line that
eases the transition from other container engines and allows the management of
pods, containers and images.  Simply put: alias docker=%{name}.
Most %{name} commands can be run as a regular user, without requiring
additional privileges.

%{name} uses Buildah(1) internally to create container images.
Both tools share image (not container) storage, hence each can use or
manipulate images (but not containers) created by the other.

%{summary}
%{name} Simple management tool for pods, containers and images

%package docker
Summary: Emulate Docker CLI using %{name}
BuildArch: noarch
Requires: %{name} = %{version}-%{release}
Provides: docker = %{version}-%{release}

%description docker
This package installs a script named docker that emulates the Docker CLI by
executes %{name} commands, it also creates links between all Docker CLI man
pages and %{name}.

%package remote
Summary: A remote CLI for Podman: A Simple management tool for pods, containers and images

%description remote
%{name}-remote provides a local client interacting with a Podman backend
node through a RESTful API tunneled through a ssh connection. In this context,
a %{name} node is a Linux system with Podman installed on it and the API
service activated.

Credentials for this session can be passed in using flags, environment
variables, or in containers.conf.

%package catatonit
Summary: A signal-forwarding process manager for containers
Requires: %{name} = %{version}-%{release}

%description catatonit
Catatonit is a /sbin/init program for use within containers. It
forwards (almost) all signals to the spawned child, tears down
the container when the spawned child exits, and otherwise
cleans up other exited processes (zombies).

This is a reimplementation of other container init programs (such as
"tini" or "dumb-init"), but uses modern Linux facilities (such as
signalfd(2)) and has no additional features.

%package plugins
Summary: Plugins for %{name}
Requires: dnsmasq

%description plugins
This plugin sets up the use of dnsmasq on a given CNI network so
that Pods can resolve each other by name.  When configured,
the pod and its IP address are added to a network specific hosts file
that dnsmasq will read in.  Similarly, when a pod
is removed from the network, it will remove the entry from the hosts
file.  Each CNI network will have its own dnsmasq instance.

%package tests
Summary: Tests for %{name}
Requires: %{name} = %{version}-%{release}
#Requires: bats  (which RHEL8 doesn't have. If it ever does, un-comment this)
Requires: nmap-ncat
Requires: httpd-tools
Requires: jq
Requires: socat
Requires: skopeo
Requires: openssl
Requires: buildah

%description tests
%{summary}

This package contains system tests for %{name}

%prep
%if 0%{?branch:1}
%autosetup -Sgit -n containers-%{name}-%{shortcommit0}
%else
%autosetup -Sgit -n %{name}-%{commit0}
%endif
tar fx %{SOURCE1}
tar fx %{SOURCE2}

mv pkg/hooks/README.md pkg/hooks/README-hooks.md

# this is shipped by skopeo: containers-common subpackage
rm -rf docs/source/markdown/containers-mounts.conf.5.md

%build
export GO111MODULE=off
export GOPATH=$(pwd):$(pwd)/_build
export CGO_CFLAGS="%{optflags} -D_GNU_SOURCE -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64"

mkdir _build
pushd _build
mkdir -p src/github.com/containers
ln -s ../../../../ src/github.com/containers/podman
popd
ln -s vendor src

rm -rf vendor/github.com/containers/storage/drivers/register/register_btrfs.go

export BUILDTAGS="systemd selinux seccomp btrfs_noversion exclude_graphdriver_devicemapper $(hack/libdm_tag.sh)"
# build date. FIXME: Makefile uses '/v2/libpod', that doesn't work here?
LDFLAGS="-X %{import_path}/libpod/define.buildInfo=$(date +%s)"
%gobuild -o bin/%{name} %{import_path}/cmd/%{name}

# build %%{name}-remote
export BUILDTAGS="remote $BUILDTAGS"
%gobuild -o bin/%{name}-remote %{import_path}/cmd/%{name}

%{__make} docs

# build catatonit
unset LDFLAGS
pushd catatonit-%{cataver}
autoreconf -fi
%configure
%{__make} %{?_smp_mflags}
# Make sure we *always* build a static binary for catatonit. Otherwise we'll break containers
# that don't have the necessary shared libs.
file catatonit | grep 'statically linked'
if [ $? != 0 ]; then
   echo "ERROR: catatonit binary must be statically linked!"
   exit 1
fi
popd

# build dnsname plugin
unset LDFLAGS
pushd dnsname-%{dnsnamever}
mkdir _build
pushd _build
mkdir -p src/github.com/containers
ln -s ../../../../ src/github.com/containers/dnsname
popd
ln -s vendor src
export GOPATH=$(pwd)/_build:$(pwd)
%gobuild -o bin/dnsname github.com/containers/dnsname/plugins/meta/dnsname
popd

%install
install -dp %{buildroot}%{_unitdir}
install -dp %{buildroot}%{_userunitdir}
install -dp %{buildroot}%{_tmpfilesdir}
PODMAN_VERSION=%{version} %{__make} PREFIX=%{buildroot}%{_prefix} \
        ETCDIR=%{buildroot}%{_sysconfdir} \
        install.bin-nobuild \
        install.remote-nobuild \
        install.man-nobuild \
        install.cni \
        install.systemd \
        install.completions \
        install.docker

# install test scripts, but not the internal helpers.t meta-test
ln -s ./ ./vendor/src # ./vendor/src -> ./vendor
install -d -p %{buildroot}/%{_datadir}/%{name}/test/system
cp -pav test/system %{buildroot}/%{_datadir}/%{name}/test/
rm -f               %{buildroot}/%{_datadir}/%{name}/test/system/*.t

# do not include docker and podman-remote man pages in main package
for file in `find %{buildroot}%{_mandir}/man[15] -type f | sed "s,%{buildroot},," | grep -v -e remote -e docker`; do
    echo "$file*" >> podman.file-list
done

# install catatonit
install -dp %{buildroot}%{_libexecdir}/catatonit
install -p catatonit-%{cataver}/catatonit %{buildroot}%{_libexecdir}/catatonit
install -dp %{buildroot}%{_libexecdir}/podman
install -dp %{buildroot}%{_datadir}/licenses/podman-catatonit
install -p catatonit-%{cataver}/COPYING %{buildroot}%{_datadir}/licenses/podman-catatonit/COPYING
ln -s %{_libexecdir}/catatonit/catatonit %{buildroot}%{_libexecdir}/podman/catatonit

# install dnsname plugin
pushd dnsname-%{dnsnamever}
%{__make} PREFIX=%{_prefix} DESTDIR=%{buildroot} install
popd

%check
%if 0%{?with_check}
# Since we aren't packaging up the vendor directory we need to link
# back to it somehow. Hack it up so that we can add the vendor
# directory from BUILD dir as a gopath to be searched when executing
# tests from the BUILDROOT dir.
ln -s ./ ./vendor/src # ./vendor/src -> ./vendor

export GOPATH=%{buildroot}/%{gopath}:$(pwd)/vendor:%{gopath}

%if ! 0%{?gotest:1}
%global gotest go test
%endif

%gotest %{import_path}/cmd/%{name}
%gotest %{import_path}/libkpod
%gotest %{import_path}/libpod
%gotest %{import_path}/pkg/registrar
%endif

%triggerpostun -- %{name} < 1.1
%{_bindir}/%{name} system renumber
exit 0

#define license tag if not already defined
%{!?_licensedir:%global license %doc}

%files -f podman.file-list
%license LICENSE
%doc README.md CONTRIBUTING.md pkg/hooks/README-hooks.md install.md transfer.md
%{_bindir}/%{name}
%{_datadir}/bash-completion/completions/%{name}
# By "owning" the site-functions dir, we don't need to Require zsh
%dir %{_datadir}/zsh/site-functions
%{_datadir}/zsh/site-functions/_%{name}
%dir %{_datadir}/fish/vendor_completions.d
%{_datadir}/fish/vendor_completions.d/%{name}.fish
%config(noreplace) %{_sysconfdir}/cni/net.d/87-%{name}-bridge.conflist
%{_unitdir}/%{name}.service
%{_unitdir}/%{name}.socket
%{_unitdir}/%{name}-auto-update.service
%{_unitdir}/%{name}-auto-update.timer
%{_userunitdir}/%{name}.service
%{_userunitdir}/%{name}.socket
%{_userunitdir}/%{name}-auto-update.service
%{_userunitdir}/%{name}-auto-update.timer
%{_usr}/lib/tmpfiles.d/%{name}.conf

%files docker
%{_bindir}/docker
%{_usr}/lib/tmpfiles.d/%{name}-docker.conf

%files remote
%license LICENSE
%{_bindir}/%{name}-remote
%{_mandir}/man1/%{name}-remote*.*
%{_datadir}/bash-completion/completions/%{name}-remote
%dir %{_datadir}/fish
%dir %{_datadir}/fish/vendor_completions.d
%{_datadir}/fish/vendor_completions.d/%{name}-remote.fish
%dir %{_datadir}/zsh
%dir %{_datadir}/zsh/site-functions
%{_datadir}/zsh/site-functions/_%{name}-remote

%files catatonit
%license COPYING
%doc README.md
%dir %{_libexecdir}/catatonit
%{_libexecdir}/catatonit/catatonit
%dir %{_libexecdir}/podman
%{_libexecdir}/podman/catatonit

%files plugins
%license dnsname-%{dnsnamever}/LICENSE
%doc dnsname-%{dnsnamever}/{README.md,README_PODMAN.md}
%{_libexecdir}/cni/dnsname

%files tests
%license LICENSE
%{_datadir}/%{name}/test

%changelog
* Tue Jun 15 2021 Jindrich Novy <jnovy@redhat.com> - 3.3.0-0.6
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/e2f51ee)
- Related: #1970747

* Mon Jun 14 2021 Jindrich Novy <jnovy@redhat.com> - 3.3.0-0.5
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/e549ca5)
- Related: #1970747

* Mon Jun 14 2021 Jindrich Novy <jnovy@redhat.com> - 3.3.0-0.4
- update podman
- Related: #1970747

* Fri Apr 16 2021 Mohan Boddu <mboddu@redhat.com> - 3.2.0-0.6
- Rebuilt for RHEL 9 BETA on Apr 15th 2021. Related: rhbz#1947937

* Tue Apr 06 2021 Jindrich Novy <jnovy@redhat.com> - 3.2.0-0.5
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/2b13c5d)

* Thu Apr 01 2021 Jindrich Novy <jnovy@redhat.com> - 3.2.0-0.4
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/12881ab)

* Wed Mar 31 2021 Jindrich Novy <jnovy@redhat.com> - 3.2.0-0.3
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/a373e2f)

* Tue Mar 30 2021 Jindrich Novy <jnovy@redhat.com> - 3.2.0-0.2
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/5eb5950)

* Mon Mar 29 2021 Jindrich Novy <jnovy@redhat.com> - 3.1.0-0.15
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/ccbe7e9)

* Fri Mar 26 2021 Jindrich Novy <jnovy@redhat.com> - 3.1.0-0.14
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/9e23e0b)

* Thu Mar 25 2021 Jindrich Novy <jnovy@redhat.com> - 3.1.0-0.13
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/e523d09)

* Wed Mar 24 2021 Jindrich Novy <jnovy@redhat.com> - 3.1.0-0.12
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/860de13)

* Tue Mar 23 2021 Jindrich Novy <jnovy@redhat.com> - 3.1.0-0.11
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/60c90c3)

* Mon Mar 22 2021 Jindrich Novy <jnovy@redhat.com> - 3.1.0-0.10
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/ebc9871)

* Fri Mar 19 2021 Jindrich Novy <jnovy@redhat.com> - 3.1.0-0.9
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/5d9b070)

* Thu Mar 18 2021 Jindrich Novy <jnovy@redhat.com> - 3.1.0-0.8
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/6f6cc1c)

* Wed Mar 17 2021 Jindrich Novy <jnovy@redhat.com> - 3.1.0-0.7
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/604459b)

* Tue Mar 16 2021 Jindrich Novy <jnovy@redhat.com> - 3.1.0-0.6
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/e7dc592)

* Mon Mar 15 2021 Jindrich Novy <jnovy@redhat.com> - 3.1.0-0.5
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/fc02d16)

* Fri Mar 12 2021 Jindrich Novy <jnovy@redhat.com> - 3.1.0-0.4
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/81737b3)

* Thu Mar 11 2021 Jindrich Novy <jnovy@redhat.com> - 3.1.0-0.3
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/e2d35e5)

* Wed Mar 10 2021 Jindrich Novy <jnovy@redhat.com> - 3.1.0-0.2
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/09473d4)

* Tue Mar 09 2021 Jindrich Novy <jnovy@redhat.com> - 3.1.0-0.1
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/789d579)

* Mon Mar 08 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.2-0.5
- remove docker man page as it was removed upstream

* Mon Mar 08 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.2-0.4
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/b7c00f2)

* Fri Mar 05 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.2-0.3
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/4e5cc6a)

* Thu Mar 04 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.2-0.2
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/87e2056)

* Wed Mar 03 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.2-0.1
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/0a40c5a)

* Mon Feb 22 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.1-2
- update to the latest content of https://github.com/containers/podman/tree/v3.0
  (https://github.com/containers/podman/commit/9a2fc37)

* Fri Feb 19 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.1-1
- update to the latest content of https://github.com/containers/podman/tree/v3.0
  (https://github.com/containers/podman/commit/7e286bc)

* Mon Feb 15 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.0-2
- update to the latest content of https://github.com/containers/podman/tree/v3.0
  (https://github.com/containers/podman/commit/797f1ea)

* Fri Feb 12 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.0-1
- update to the latest content of https://github.com/containers/podman/tree/v3.0
  (https://github.com/containers/podman/commit/ddd8a17)

* Wed Feb 10 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.29rc2
- update to the latest content of https://github.com/containers/podman/tree/v3.0
  (https://github.com/containers/podman/commit/2b89fe7)

* Tue Feb 09 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.28rc2
- update to the latest content of https://github.com/containers/podman/tree/v3.0
  (https://github.com/containers/podman/commit/a5ab59e)

* Sat Feb 06 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.27rc2
- update to the latest content of https://github.com/containers/podman/tree/v3.0
  (https://github.com/containers/podman/commit/288fb68)

* Thu Feb 04 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.26rc2
- update to the latest content of https://github.com/containers/podman/tree/v3.0
  (https://github.com/containers/podman/commit/82081e8)

* Wed Feb 03 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.25rc2
- update to the latest content of https://github.com/containers/podman/tree/v3.0
  (https://github.com/containers/podman/commit/978c005)

* Tue Feb 02 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.24rc2
- update to the latest content of https://github.com/containers/podman/tree/v3.0
  (https://github.com/containers/podman/commit/67d48c5)

* Mon Feb 01 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.23rc2
- require oci-runtime to assure either crun or runc is pulled in via
  dependencies
- Resolves: #1923547

* Sun Jan 31 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.22rc2
- update to the latest content of https://github.com/containers/podman/tree/v3.0
  (https://github.com/containers/podman/commit/745fa4a)

* Wed Jan 27 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.21rc1
- update to the latest content of https://github.com/containers/podman/tree/v3.0
  (https://github.com/containers/podman/commit/4dbb58d)

* Tue Jan 26 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.20rc1
- update to the latest content of https://github.com/containers/podman/tree/v3.0
  (https://github.com/containers/podman/commit/dc2f4c6)

* Tue Jan 26 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.19rc1
- update to the latest content of https://github.com/containers/podman/tree/v3.0
  (https://github.com/containers/podman/commit/469c203)

* Mon Jan 18 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.18rc1
- switch from master to release candidate (3.0.0-rc1)

* Fri Jan 08 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.17
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/78cda71)

* Thu Jan 07 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.16
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/355e387)

* Wed Jan 06 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.15
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/ffe2b1e)

* Mon Jan 04 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.14
- re-disable LTO as it sill fails even with GCC 11
- Related: #1904567

* Mon Jan 04 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.13
- attempt to build with gcc11
- Related: #1904567

* Mon Jan 04 2021 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.12
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/23f25b8)

* Thu Dec 10 2020 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.11
- update to https://github.com/containers/dnsname/releases/tag/v1.1.1

* Wed Dec 09 2020 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.10
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/dd295f2)

* Mon Dec 07 2020 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.9
- disable LTO to fix build

* Mon Dec 07 2020 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.8
- use dedicated macro to build only on supported arches

* Mon Dec 07 2020 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.7
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/0c96731)

* Mon Dec 07 2020 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.6
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/0c2a43b)

* Sat Dec 05 2020 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.5
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/8e83799)

* Fri Dec 04 2020 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.4
- update to the latest content of https://github.com/containers/podman/tree/master
  (https://github.com/containers/podman/commit/70284b1)

* Thu Dec 03 2020 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.3
- attempt to fix gating tests with patch from Matt Heon

* Thu Dec 03 2020 Jindrich Novy <jnovy@redhat.com> - 3.0.0-0.1
- update NVR to reflect the development version

* Thu Dec 03 2020 Jindrich Novy <jnovy@redhat.com> - 2.2.0-2
- switch to master branch
- remove varlink support
- simplify spec

* Tue Dec 01 2020 Jindrich Novy <jnovy@redhat.com> - 2.2.0-1
- update to https://github.com/containers/podman/releases/tag/v2.2.0

* Thu Nov 05 2020 Jindrich Novy <jnovy@redhat.com> - 2.1.1-11
- fix branch name setup

* Wed Nov 04 2020 Jindrich Novy <jnovy@redhat.com> - 2.1.1-10
- attempt to fix linker error with golang-1.15
- add Requires: httpd-tools to tests, needed to work around
  missing htpasswd in docker registry image, thanks to Ed Santiago

* Wed Nov 04 2020 Jindrich Novy <jnovy@redhat.com> - 2.1.1-9
- simplify spec
- use shortcommit ID in all tarball names

* Fri Oct 23 2020 Jindrich Novy <jnovy@redhat.com> - 2.1.1-8
- use shortcommit ID in branch tarball name

* Thu Oct 22 2020 Jindrich Novy <jnovy@redhat.com> - 2.1.1-7
- add gating test files

* Wed Oct 21 2020 Jindrich Novy <jnovy@redhat.com> - 2.1.1-6
- fix the tarball reference for consumption directly from upstream branch

* Sat Oct 17 2020 Jindrich Novy <jnovy@redhat.com> - 2.1.1-5
- allow to build directly from upstream branch or release
- make varlink disabled by default

* Thu Oct 15 2020 Jindrich Novy <jnovy@redhat.com> - 2.1.1-4
- add jnovy to gating test results recipients

* Thu Oct 15 2020 Jindrich Novy <jnovy@redhat.com> - 2.1.1-3
- update gating tests

* Wed Sep 30 2020 Jindrich Novy <jnovy@redhat.com> - 2.1.1-2
- fix the container-selinux boolean dependency

* Mon Sep 28 2020 Jindrich Novy <jnovy@redhat.com> - 2.1.1-1
- update to https://github.com/containers/podman/releases/tag/v2.1.1

* Wed Sep 23 2020 Jindrich Novy <jnovy@redhat.com> - 2.1.0-1
- update to https://github.com/containers/podman/releases/tag/v2.1.0

* Wed Sep 23 2020 Jindrich Novy <jnovy@redhat.com> - 2.0.6-5
- allow to be built on different than RHEL OSes

* Tue Sep 22 2020 Jindrich Novy <jnovy@redhat.com> - 2.0.6-4
- include dnsname plugin
Resolves: #1877865

* Tue Sep 22 2020 Jindrich Novy <jnovy@redhat.com> - 2.0.6-3
- require container-selinux only when selinux-policy is installed
Related: #1881218

* Mon Sep 21 2020 Jindrich Novy <jnovy@redhat.com> - 2.0.6-2
- use commit ID to refer to the upstream tarball

* Fri Sep 18 2020 Jindrich Novy <jnovy@redhat.com> - 2.0.6-1
- update to https://github.com/containers/podman/releases/tag/v2.0.6

* Thu Sep 17 2020 Jindrich Novy <jnovy@redhat.com> - 2.0.5-5
- update to podman-2.0.5 in rhel8 branch

